{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

import Control.Monad
  ( Monad ((>>)),
    filterM,
    mapM_,
    return,
    when,
    (=<<),
    (>>),
    (>>=),
  )
import Control.Monad.IO.Class (liftIO)
import Data.Bool (Bool (False, True))
import Data.Eq (Eq, (==))
import Data.Function (const, id, ($), (.))
import Data.Functor ((<$>))
import Data.List
  ( foldl,
    length,
    map,
    unlines,
    zipWith,
    (++),
  )
import qualified Data.Map as M
import Data.Monoid (All, Endo, Monoid (mconcat, mempty), (<>))
import Data.Ord (Ord)
import Data.String (String)
import Data.Tuple (uncurry)
-- import System.Taffybar.Support.PagerHints (pagerHints)
-- import XMonad.Config.Prime (Rectangle)
import GHC.Enum (Bounded, Enum)
import GHC.Num (Integer, fromInteger, (+), (-))
import GHC.Real (Fractional ((/)), div, fromIntegral, (%))
import Graphics.X11.Types
  ( KeyMask,
    KeySym,
    Window,
    mod4Mask,
    xK_0,
    xK_1,
    xK_2,
    xK_3,
    xK_4,
    xK_5,
    xK_6,
    xK_7,
    xK_8,
    xK_9,
    xK_Escape,
    xK_F1,
  )
import System.IO (IO, IOMode (AppendMode), hClose, hPutStr, hPutStrLn, putStrLn, withFile)
import Text.Show (Show (show))
import XMonad
  ( Event,
    MonadReader (ask),
    Rectangle (rect_height, rect_width),
    WindowAttributes,
    asks,
    config,
    display,
    getGeometry,
    gets,
    modify,
    screenRect,
    theRoot,
    wa_height,
    wa_width,
    wa_x,
    wa_y,
    withDisplay,
    withWindowAttributes,
  )
import XMonad.Actions.CopyWindow (copyToAll, killAllOtherCopies)
import XMonad.Actions.CycleWS
  ( nextScreen,
    nextWS,
    prevScreen,
    prevWS,
    shiftNextScreen,
    shiftPrevScreen,
    shiftToNext,
    shiftToPrev,
  )
import XMonad.Actions.EasyMotion
  ( ChordKeys (AnyKeys),
    EasyMotionConfig (..),
    selectWindow,
    textSize,
  )
import XMonad.Actions.FloatKeys (keysMoveWindowTo)
import XMonad.Actions.GridSelect
  ( GSConfig (..),
    bringSelected,
    buildDefaultGSConfig,
    colorRangeFromClassName,
  )
import XMonad.Actions.Minimize (minimizeWindow)
import XMonad.Actions.OnScreen (onlyOnScreen)
import XMonad.Actions.SpawnOn (manageSpawn, spawnOn)
import XMonad.Actions.WithAll (killAll)
import XMonad.Config (Default (def))
import XMonad.Config.Prime (getWindowAttributes)
import XMonad.Core
  ( Layout,
    LayoutClass,
    ManageHook,
    Query,
    WindowSet,
    X,
    XConfig (..),
    io,
    keys,
    runQuery,
    spawn,
    whenJust,
    windowset,
  )
import XMonad.Hooks.BorderPerWindow (actionQueue, defineBorderWidth)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.InsertPosition (Focus (Newer), Position (Master), insertPosition)
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks)
import XMonad.Hooks.ManageHelpers
  ( Side (..),
    doCenterFloat,
    doFloatDep,
    doFullFloat,
    doSideFloat,
    isDialog,
    isFullscreen,
    isInProperty,
    transience',
  )
import XMonad.Hooks.Minimize (minimizeEventHook)
import XMonad.Hooks.OnPropertyChange (onXPropertyChange)
import XMonad.Hooks.Rescreen (RescreenConfig (..), rescreenHook)
import XMonad.Hooks.ServerMode (serverModeEventHookF)
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Hooks.ShowWName (flashName)
-- import XMonad.Layout.Magnifier (magnifier)

import XMonad.Hooks.StatusBar (StatusBarConfig (..), statusBarGeneric, withSB, xmonadPropLog)
import XMonad.Hooks.TaffybarPagerHints (pagerHints)
import XMonad.Hooks.WindowSwallowing (swallowEventHook)
import XMonad.Layout
  ( ChangeLayout (NextLayout),
    Full (Full),
    Mirror (Mirror),
    Tall (Tall),
    (|||),
  )
import qualified XMonad.Layout.BoringWindows as BW
import XMonad.Layout.DraggingVisualizer (draggingVisualizer)
import XMonad.Layout.Fullscreen
  ( fullscreenEventHook,
    fullscreenManageHook,
    fullscreenSupport,
    fullscreenSupportBorder,
  )
import XMonad.Layout.Grid (Grid (Grid, GridRatio))
import XMonad.Layout.IndependentScreens (withScreen)
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.LayoutScreens (layoutScreens, layoutSplitScreen)
import XMonad.Layout.Maximize (Maximize, maximizeRestore, maximizeWithPadding)
import XMonad.Layout.Minimize (minimize)
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.NoFrillsDecoration (Theme, noFrillsDeco)
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Renamed (Rename (Replace), renamed)
import XMonad.Layout.Roledex (Roledex (Roledex))
import XMonad.Layout.ShowWName
  ( SWNConfig (swn_bgcolor, swn_color, swn_fade, swn_font),
    showWName',
  )
import XMonad.Layout.Simplest (Simplest (Simplest))
import XMonad.Layout.Spacing (Border (..), Spacing (..), spacingRaw)
import XMonad.Layout.Stoppable (stoppable)
import XMonad.Layout.SubLayouts
  ( GroupMsg (Merge, MergeAll, UnMerge),
    onGroup,
    pullGroup,
    subLayout,
  )
import XMonad.Layout.Tabbed
  ( Direction2D (D, L, R, U),
    Theme (..),
    addTabs,
    shrinkText,
  )
import XMonad.Layout.WindowNavigation (windowNavigation)
import XMonad.Layout.WindowSwitcherDecoration (windowSwitcherDecorationWithButtons)
import XMonad.Main (xmonad)
import XMonad.ManageHook
  ( className,
    composeAll,
    doF,
    doIgnore,
    doShift,
    idHook,
    liftX,
    title,
    (-->),
    (<&&>),
    (<+>),
    (<||>),
    (=?),
    --    (~?),
  )
import XMonad.Operations
  ( kill,
    killWindow,
    refresh,
    rescreen,
    sendMessage,
    --    unGrab,
    windows,
    withFocused,
  )
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (mkNamedKeymap)
import XMonad.Util.Font (Align (AlignLeft, AlignRightOffset))
import XMonad.Util.NamedActions
  ( NamedAction,
    addDescrKeys',
    addName,
    noName,
    showKmSimple,
  )
import XMonad.Util.NamedScratchpad
  ( NamedScratchpad (NS),
    customFloating,
    namedScratchpadAction,
    namedScratchpadManageHook,
  )
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.Ungrab (unGrab)

type KeyBind = String

main :: IO ()
main                       = do
  xmonad
    . addDescrKeys' ((mod4Mask, xK_F1), showKeybinds) myKeys
    . docks
    . ewmhFullscreen
    . ewmh
    . pagerHints
    . actionQueue
    -- . fullscreenSupport
    . fullscreenSupportBorder
    . rescreenHook rsConfig
    . withSB statusBarConfigs
    $ def
      { borderWidth        = 3,
        -- clientMask      = "",
        clickJustFocuses   = False,
        focusedBorderColor = selectedBackground colorTheme,
        focusFollowsMouse  = True,
        -- handleEventHook = debugStackFullEventHook,
        -- handleEventHook = dynamicPropertyChange "WM_NAME" myHandleEventHook,
        handleEventHook    = myHandleEventHook,
        keys               = const M.empty,
        layoutHook         = myLayoutHook,
        -- logHook         = fadeInactiveLogHook 0.8,
        -- logHook         = debugStackFullLogHook,
        -- logHook         = showWNameLogHook swnConfig, -- buggy! wrong size
        logHook            = return (),
        manageHook         = myManageHook,
        modMask            = mod4Mask,
        normalBorderColor  = background colorTheme,
        startupHook        = myStartupHook,
        terminal           = myTerminal,
        workspaces         = myWorkspaces
        -- workspaces      = withScreen 2 myWorkspaces
      }
  where
    -- myLayoutHook = avoidStruts allLayouts
    myLayoutHook        = showWName' swnConfig . avoidStruts $ allLayouts
    allLayouts =
      onWorkspace (show Alpha) mainLayout $
        onWorkspace (show Beta) mainLayout $
          onWorkspace (show Delta) mainLayout $
            onWorkspace (show Lambda) mainLayout $
              onWorkspace (show Pi) mediaFull $
                onWorkspace (show Sigma) commsFull $
                  onWorkspace (show Omega) (grid ||| roledex) full
    mainLayout          = tall ||| wide ||| full
    basicTall           = Tall 1 (3 / 100) (1 / 2)
    basicWide           = Mirror basicTall
    tall =
      renamed [Replace "T"]
        . maximizeWithPadding'
        . minimize
        . BW.boringWindows
        . windowSwitcherDecorationWithButtons'
        -- . magnifier
        . draggingVisualizer
        . windowNavigation
        . addTabs'
        . subLayout'
        . spacing'
        $ basicTall
    wide =
      renamed [Replace "W"]
        . maximizeWithPadding'
        . minimize
        . BW.boringWindows
        . windowSwitcherDecorationWithButtons'
        . draggingVisualizer
        . windowNavigation
        . addTabs'
        . subLayout'
        . spacing'
        $ basicWide
    full =
      renamed [Replace "F"]
        -- . noBorders
        . maximizeWithPadding'
        . minimize
        . BW.boringWindows
        . windowSwitcherDecorationWithButtons'
        . spacing'
        $ Full
    mediaFull =
      renamed [Replace "F"]
        . maximizeWithPadding'
        . minimize
        . BW.boringWindows
        -- . windowSwitcherDecorationWithButtons'
        -- . spacing'
        $ Full
    commsFull =
      renamed [Replace "F"]
        . maximizeWithPadding'
        . minimize
        . BW.boringWindows
        -- . windowSwitcherDecorationWithButtons'
        . spacing'
        $ Full
    roledex =
      renamed [Replace "R"] . stoppable . spacing' $ Roledex
    grid =
      renamed [Replace "G"] . stoppable . spacing' $ Grid
    gap                 = 4
    spacing' =
      spacingRaw False (Border gap 0 gap 0) True (Border 0 gap 0 gap) True
    maximizeWithPadding' =
      maximizeWithPadding $ fromInteger gap
    windowSwitcherDecorationWithButtons' =
      windowSwitcherDecorationWithButtons shrinkText titleBarTheme
    subLayout'          = subLayout [] Simplest
    addTabs'            = addTabs shrinkText tabTheme
    mkSBConfig (log, startup, cleanup) =
      def
        { sbLogHook     = log,
          sbStartupHook = spawn startup,
          sbCleanupHook = spawn cleanup
        }
    statusBarConfigs =
      mconcat $
        mkSBConfig
          <$> [ (mempty, "run-taffybar.sh", "killall taffybar-linux-x86_64"),
                (mempty, "run-xmobar.sh", "killall xmobar"),
                (mempty, "run-trayer.sh", "killall trayer")
              ]

myHandleEventHook :: Event -> X All
myHandleEventHook = mconcat eventHooks
  where
    eventHooks =
      [ fullscreenEventHook,
        onXPropertyChange "WM_NAME" myDynamicManageHook,
        minimizeEventHook,
        swallowEventHook classNamesToSwallow $ return True
      ]
        <> serverModeEventHooks
    refreshHook = const refreshXMonad
    reloadHook = const reloadXMonad
    namedScratchpadHook = namedScratchpadAction myNamedScratchpads
    classNamesToSwallow =
      foldl
        swallow
        (className =? "Terminator")
        [ "Alacritty",
          "alacritty-xplr",
          "alacritty-tuir",
          "alacritty-zellij",
          "XTerm"
        ]
    swallow s i = s <||> className =? i
    serverModeEventHooks =
      bindEventHook
        <$> [ ("XMONAD_NAMED_SCRATCHPAD", namedScratchpadHook),
              ("XMONAD_REFRESH", refreshHook),
              ("XMONAD_RELOAD", reloadHook)
            ]
    bindEventHook = uncurry serverModeEventHookF

myKeys :: XConfig Layout -> [((KeyMask, KeySym), NamedAction)]
myKeys = ($ bindKey <$> allKeys) . mkNamedKeymap
  where
    bindKey (kb, d, a) = (kb, addName d $ unGrab >> a)
    bindWsKey ws n =
      [ ("M-" <> show n, "switch to " <> ws <> " workspace", windows . W.view $ ws),
        ("M-S-" <> show n, "send window to " <> ws <> " workspace", windows . W.shift $ ws),
        ("M-C-" <> show n, "switch to " <> ws <> " workspace greedily", windows . W.greedyView $ ws)
      ]
    bindPromptKey (d, a) n = ("M-<F" <> show n <> ">", d, a)
    allKeys =
      [ -- applications
        -- ("M-e", "launch file manager", spawn "run-alacritty-xplr.sh ~"),
        ("M-e", "launch file manager", spawn "run-pcmanfm-qt.sh"),
        ("M-S-e", "launch quick file manager", spawn "rofi -show filebrowser -display-filebrowser ''"),
        ("M-M1-e", "launch persistent file manager", namedScratchpadAction myNamedScratchpads "scratchpad-file-manager"),
        ("M-i", "launch firefox web browser", spawn "run-firefox.sh"),
        -- ("M-S-i", "launch chrome web browser", spawn "run-google-chrome.sh"),
        ("M-t", "launch text editor", spawn "emacsclient -c -a 'emacs'"),
        -- ("M-t", "launch text editor", spawn "emacs"),
        ("M-<Return>", "launch terminator", spawn "run-terminator.sh"),
        ("M-S-<Return>", "launch alacritty with zellij", spawn "run-alacritty-zellij.sh"),
        ("M-<KP_Enter>", "launch persistent terminal", namedScratchpadAction myNamedScratchpads "scratchpad-terminal"),
        -- window operations
        ("M-w", "close window", killAllOtherCopies >> spawn "kill-window.sh"),
        ("M-C-w", "close selected window", selectWindow redEmConfig >>= (`whenJust` windows . W.focusWindow) >> spawn "kill-window.sh"),
        ("M-S-w", "close all windows", killAll),
        ("M-<Page_Down>", "minimize window", withFocused minimizeWindow),
        ("M-<Page_Up>", "maximize window", withFocused (sendMessage . maximizeRestore)),
        ("M-<Backspace>", "promote window to master", windows W.swapMaster),
        ("M-C-<Backspace>", "promote selected window to master", selectWindow blueEmConfig >>= (`whenJust` windows . W.focusWindow) >> windows W.swapMaster),
        -- ("M-<Tab>",  "switch to next window" , return ()),
        ("M-C-<Tab>", "switch to selected window", selectWindow blueEmConfig >>= (`whenJust` windows . W.focusWindow)),
        ("M-M1-<Tab>", "switch to any window", spawn "rofi -show window -display-window ''"),
        ("M-<Insert>", "float or sink a window", withFocused toggleFloat),
        ("M-S-<Insert>", "copy window to all workspaces", windows copyToAll),
        ("M-<Up>", "swap window position from the top", windows W.swapUp),
        ("M-<Down>", "swap window position from the bottom", windows W.swapDown),
        ("M-C-<Left>", "group a window from the left", sendMessage $ pullGroup L),
        ("M-C-<Right>", "group a window from the right", sendMessage $ pullGroup R),
        ("M-C-<Up>", "group a window from the top", sendMessage $ pullGroup U),
        ("M-C-<Down>", "group a window from the bottom", sendMessage $ pullGroup D),
        ("M-C-u", "remove window from group", withFocused (sendMessage . UnMerge)),
        -- monitor operations
        ("M-<KP_Left>", "switch to next monitor", prevScreen),
        ("M-<KP_Right>", "switch to previous monitor", nextScreen),
        -- layout operations
        ("M-<Space>", "switch to next layout", sendMessage NextLayout),
        -- workspace operations
        ("M-S-<Left>", "send window to previous workspace", shiftPrevScreen >> prevScreen),
        ("M-S-<Right>", "send window to next workspace", shiftNextScreen >> nextScreen),
        ("M-S-`", "send window to current workspace", bringSelected gsConfig),
        -- utilities
        ("M-<Print>", "show screenshot menu", spawn "rofi-screenshot-prompt.sh"),
        ("M-S-<Print>", "start/save screenrecorder", spawn "start-save-screenrecorder.sh"),
        ("M-<Pause>", "launch screensaver", spawn "lxqt-leave --lockscreen"),
        ("M-S-<Pause>", "sleep", spawn "lxqt-leave --suspend"),
        ("M-<Escape>", "show leave menu", spawn "lxqt-leave"),
        ("M-S-<Delete>", "launch persistent task manager", namedScratchpadAction myNamedScratchpads "scratchpad-htop"),
        ("M-M1-<Delete>", "launch task manager", spawn "qps"),
        ("C-M1-v", "show clipboard menu", spawn "clipcat-menu"),
        -- xf86 keys
        ("<XF86AudioLowerVolume>", "decrease audio volume", spawn "pulsemixer --change-volume -5"),
        ("<XF86AudioMicMute>", "toggle microphone on/off", spawn "mic-volume.sh mute"),
        ("<XF86AudioMute>", "toggle audio on/off", spawn "pulsemixer --toggle-mute"),
        ("<XF86AudioNext>", "play next music track", spawn "mpc next"),
        ("<XF86AudioPlay>", "toggle music play/pause", spawn "toggle-mpc-play.sh"),
        ("<XF86AudioPrev>", "play previous music track", spawn "mpc prev"),
        ("<XF86AudioRaiseVolume>", "increase audio volume", spawn "pulsemixer --change-volume +5"),
        ("<XF86AudioRecord>", "record audio stream", spawn "audio-recorder"),
        ("<XF86Bluetooth>", "configure bluetooth", spawn "blueman-manager"),
        -- ("<XF86Calculator>", "launch calculator", spawn "speedcrunch"),
        ("<XF86Calculator>", "launch calculator", spawn "galculator"),
        ("<XF86Calendar>", "launch calendar", spawn "thunderbird -calendar"),
        -- ("<XF86Calendar>", "launch calendar", spawn "betterbird -calendar"),
        ("<XF86Documents>", "show ~/Documents directory", spawn "pcmanfm-qt ~/Documents"),
        ("<XF86Display>", "configure monitor display", spawn "arandr"),
        ("<XF86Eject>", "eject external storage device", spawn "rofi-eject-prompt.sh"),
        ("<XF86Excel>", "launch spreadsheet", spawn "libreoffice --calc"),
        ("<XF86Explorer>", "launch file manager", spawn "run-pcmanfm-qt.sh"),
        -- ("<XF86Game>", spawn ""), -- TODO: use rofi-game-prompt
        -- ("<XF86HomePage>", spawn ""), -- TODO: use wtf
        -- ("<XF86LightBulb>", "toggle lightbulb", spawn ""), -- TODO: use tapo l530e script
        ("<XF86LogOff>", "logout user", spawn "lxqt-leave --logout"),
        ("<XF86Mail>", "launch mail client", spawn "thunderbird"),
        ("<XF86Memo>", "launch notebook", spawn "run-joplin.sh"),
        ("<XF86Messenger>", "launch messenger", spawn "caprine"),
        ("<XF86Music>", "launch music player", spawn "plattenalbum"),
        ("<XF86News>", "launch news feeds", spawn "liferea"),
        ("<XF86Phone>", "show phone screen ", spawn "run-scrcpy.sh"),
        ("<XF86Pictures>", "launch picture gallery", spawn "lximage-qt ~/Pictures"),
        ("<XF86PowerDown>", "suspend machine", spawn "lxqt-leave --reboot"),
        ("<XF86PowerOff>", "shutdown machine", spawn "lxqt-leave --shutdown"),
        ("<XF86Refresh>", "refresh window manager", refreshXMonad),
        ("<XF86Reload>", "reload window manager", reloadXMonad),
        ("<XF86ScreenSaver>", "launch screensaver", spawn "lxqt-leave --lockscreen"),
        ("<XF86Search>", "show search menu", spawn "rofi-search-prompt.sh"),
        ("<XF86Sleep>", "sleep", spawn "lxqt-leave --suspend"),
        ("<XF86SplitScreen>", "split screen", layoutSplitScreen 4 Grid),
        -- ("<XF86Start>", spawn ""), -- TODO: use app menu or S-F2
        ("<XF86Terminal>", "launch terminal", spawn "run-terminator.sh"),
        ("<XF86Tools>", "launch music player", spawn "Mpdevil"), -- keyboard bug ?!
        ("<XF86TouchpadToggle>", "toggle touchpad", spawn "toggle-trackpad.sh"),
        ("<XF86Video>", "show ~/Videos directory", spawn "pcmanfm-qt ~/Videos"),
        ("<XF86WWW>", "launch web browser", spawn "run-firefox.sh"),
        ("<XF86WebCam>", "launch web camera", spawn "webcamoid"),
        ("<XF86Word>", "launch word processor", spawn "libreoffice --writer"),
        -- experimental
        -- ("M-l", goToSelected def),
        ("M-C-m", "*", withFocused (sendMessage . MergeAll)),
        ("M-C-.", "*", onGroup W.focusUp'),
        ("M-C-,", "*", onGroup W.focusDown'),
        -- ("M-k", "*", flashName swnConfig),
        ("M-k", "*", getRootWindowDimensions),
        ("M-l", "*", layoutSplitScreen 4 Grid),
        ("M-j", "*", layoutScreens 4 Full),
        ("<Menu>", "*", flashName swnConfig),
        -- ("M-l", "*", refresh >> rescreen),
        ("M-M1-<Left>", "*", shiftToPrev >> prevWS),
        ("M-M1-<Right>", "*", shiftToNext >> nextWS)
        -- ("M-l", gridselectWorkspace , gsConfig1 gsColorizer $ (\ws -> W.greedyView ws . W.shift ws)),
        -- ("M-S-2", onGroup W.focusDown')
      ]
        <> wsKeys
        <> promptKeys
    wsKeys =
      mconcat $ zipWith bindWsKey myWorkspaces [1 ..]
    promptKeys =
      zipWith
        bindPromptKey
        [ ("show keybinds menu", return ()), -- F1
          ("show application menu", spawn "rofi-app-prompt.sh"), -- F2
          ("show project menu", spawn "rofi-project-prompt.sh"), -- F3
          ("show chatgpt prompt", spawn "rofi-gpt-prompt.sh"), -- F4
          ("show ssh menu", spawn "rofi -show-icons -show ssh -display-ssh ' ssh'"), -- F5
          ("show vpn menu", spawn "rofi-vpn-prompt.sh"), -- F6
          ("show manpage menu", spawn "rofi-man-prompt.sh"), -- F7
          ("show search menu", spawn "rofi-search-prompt.sh"), -- F8
          ("show xf86 keys menu", spawn "rofi-run-command-prompt.sh"), -- F9
          ("show virtual machine menu", spawn "rofi-virtualbox-prompt.sh"), -- F10
          ("show online radio menu", spawn "rofi-online-radio-prompt.sh"), -- F11
          ("show online tv menu", spawn "rofi-online-tv-prompt.sh") -- F12
        ]
        [1 ..]

myManageHook :: Query (Endo WindowSet)
myManageHook =
  insertPosition Master Newer
    <> fullscreenManageHook
    <> manageSpawn
    <> allManageHooks
      <+> namedScratchpadManageHook myNamedScratchpads -- TODO: insertPosition doesn't work with copyToAll
      -- myManageHook  = manageSpawn <> allManageHooks <+> namedScratchpadManageHook myNamedScratchpads
  where
    allManageHooks =
      composeAll $
        mconcat [preHooks, otherHooks, classNameAndTitleHooks, classNameHooks, titleHooks, postHooks]
    -- doFloatToAll  = doCenterFloat <+> doF copyToAll
    -- doFloatToAll  = doCenterFloat <+> doF copyToAll <+> doF W.focusUp
    doSideFloatToAll = doSideFloat CE <+> doF copyToAll
    isSplash = isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH"
    isPictureInPicture = isInProperty "_NET_WM_NAME" "Picture in picture"
    preHooks =
      [ isDialog --> doCenterFloat,
        isFullscreen --> doFullFloat,
        isPictureInPicture --> defineBorderWidth 0,
        isPictureInPicture --> doShift (show Pi),
        isSplash --> defineBorderWidth 0,
        isSplash --> doCenterFloat
      ]
    postHooks =
      [ transience',
        manageDocks
      ]
    classNameHooks =
      uncurry ((-->) . (className =?))
        <$> [ (".arandr-wrapped", doCenterFloat),
              (".barrier-wrapped", doCustomCenterFloat), -- not working
              (".blueman-manager-wrapped", doCustomCenterFloat),
              (".blueman-services-wrapped", doCustomCenterFloat),
              -- (".protonvpn-app-wrapped", doCenterFloat),
              (".protonvpn-app-wrapped", doAbsoluteCenterFloat),
              (".scrcpy-wrapped", doSideFloat CE),
              ("Audio-recorder", doCenterFloat),
              ("Barrier", doCustomCenterFloat), -- not working
              ("Bitwarden", doCustomCenterFloat),
              ("Blender", doShift $ show Pi),
              ("Caprine", doShift $ show Sigma),
              ("Droidcam", doCenterFloat),
              ("GitHub Desktop", doShift $ show Delta),
              ("Gimp-2.10", doShift $ show Pi),
              ("Hypnotix.py", doShift $ show Pi),
              ("Liferea", doShift $ show Pi),
              ("Lxappearance", doCustomCenterFloat),
              ("Mictray", doCenterFloat),
              ("Mpdevil", doCenterFloat),
              ("Nvidia-settings", doCustomCenterFloat),
              ("Nvidia-settings", doShift $ show Pi),
              ("Nm-connection-editor", doCustomCenterFloat),
              ("Qalculate-gtk", doCustomCenterFloat),
              ("Qalculate-gtk", doShift $ show Delta),
              ("Rtng-bookmark-editor", doCustomCenterFloat),
              ("SimpleScreenRecorder", doCustomCenterFloat),
              ("Slack", doShift $ show Sigma),
              ("SoundWireServer", doCenterFloat),
              ("SpeedCrunch", doCustomCenterFloat),
              ("Taffybar-linux-x86_64", defineBorderWidth 0),
              ("TradingView", doShift $ show Pi),
              ("Yad", doCustomCenterFloat),
              -- ("Sxiv", doCustomCenterFloat),
              ("alacritty-bluetoothctl", doCenterFloat),
              ("alacritty-calcurse", doCustomCenterFloat),
              -- ("alacritty-newsboat", doCustomCenterFloat),
              ("alacritty-nmtui", doCenterFloat),
              ("alacritty-pcalc", doCenterFloat),
              ("alacritty-pulsemixer", doCenterFloat),
              ("alacritty-steam-tui", doCustomCenterFloat),
              ("alacritty-stig", doCustomCenterFloat),
              ("alacritty-termscp", doCustomCenterFloat),
              -- ("alacritty-tuir", doCustomCenterFloat),
              ("alacritty-weechat", doCustomCenterFloat),
              ("alacritty-wego", doCustomCenterFloat),
              -- ("alacritty-xplr", doCustomCenterFloat),
              ("betterbird", doShift $ show Sigma),
              ("dhewm3", defineBorderWidth 0),
              ("dhewm3", doShift $ show Pi),
              ("discord", doShift $ show Sigma),
              -- ("htop", doCenterFloat),
              ("kdeconnect.app", doCenterFloat),
              ("kdeconnect.sms", doCenterFloat),
              ("lucasr.exe", doShift $ show Pi),
              ("lximage-qt", doShift $ show Pi),
              ("lxqt-archiver", doCustomCenterFloat),
              ("lxqt-config", doCustomCenterFloat),
              -- ("mage-client-MageFrame", doShift $ show Pi),
              ("mpv", defineBorderWidth 0),
              ("mpv", doShift $ show Pi),
              ("pavucontrol", doCustomCenterFloat),
              ("plattenalbum", doShift $ show Pi),
              ("qps", doCustomCenterFloat),
              ("teams-for-linux", doShift $ show Sigma),
              ("thunderbird", doShift $ show Sigma),
              ("vlc", defineBorderWidth 0),
              ("vlc", doShift $ show Pi),
              ("zoom", doShift $ show Sigma),
              --
              ("confirm", doCenterFloat),
              ("download", doCenterFloat),
              ("error", doCenterFloat),
              ("file_progress", doCenterFloat),
              ("notification", doIgnore),
              ("splash", doCenterFloat),
              ("splash", defineBorderWidth 0)
            ]
    titleHooks =
      uncurry ((-->) . (title =?))
        <$> [ ("About Betterbird", doCenterFloat),
              ("About Mozilla Firefox", doCenterFloat),
              ("Barrier", doCustomCenterFloat), -- not working
              ("Netflix", doShift $ show Pi),
              ("Nitrogen", doCenterFloat),
              ("Picture in picture", defineBorderWidth 0),
              ("Picture in picture", doShift $ show Pi),
              ("Rambox", doShift $ show Sigma),
              ("Steam", doShift $ show Pi),
              ("Steam - News", doShift $ show Pi),
              ("Steam - News", doCustomCenterFloat),
              ("Transmission", doCustomCenterFloat),
              ("Trash", doCustomCenterFloat),
              ("galculator", doCustomCenterFloat),
              ("galculator", doShift $ show Delta),
              ("gxmessage", doCenterFloat),
              ("xmessage", doCenterFloat)
            ]
    classNameAndTitleHooks =
      (\(c, t, h) -> className =? c <&&> title =? t --> h)
        <$> [ ("DBeaver", "About  ", doCenterFloat), -- yes there are 2 spaces!
              ("DBeaver", "Connect to a database ", doCustomCenterFloat), -- yes there is a space!
              ("DBeaver", "Exit DBeaver ", doCenterFloat), -- yes there is a space!
              ("DBeaver", "Preferences ", doCustomCenterFloat), -- yes there is a space!
              ("DBeaver", "Tip of the day ", doCustomCenterFloat), -- yes there is a space!
              ("Slack", "Help", doCustomCenterFloat),
              ("Steam", "Friends List", doCustomCenterFloat),
              ("Steam", "Friends List", doShift $ show Pi),
              ("VirtualBox", "Network Operations Manager", doCenterFloat),
              ("discord", "Discord Updater", defineBorderWidth 0),
              ("firefox", "Library", doCustomCenterFloat),
              ("firefox", "Mag-sign in - Google Accounts — Mozilla Firefox", doCustomCenterFloat),
              ("firefox", "Picture-in-Picture", defineBorderWidth 0),
              ("firefox", "Picture-in-Picture", doShift $ show Pi),
              -- ("firefox", "Picture-in-Picture", doSideFloatToAll), -- TODO: not working when insertPosition is enabled
              -- ("firefox", "Picture-in-Picture", doSideFloat CE),
              ("firefox", "Sign in - Google Accounts — Mozilla Firefox", doCustomCenterFloat),
              ("lxqt-archiver", "Progress", doCustomCenterFloat),
              ("terminator", "Rename Window", doCenterFloat),
              ("zoom", "Meeting", defineBorderWidth 0),
              ("zoom", "Meeting", doShift $ show Pi),
              ("zoom", "Webinar", defineBorderWidth 0),
              ("zoom", "Webinar", doShift $ show Pi),
              ("zoom", "Settings", doCustomCenterFloat)
            ]
    otherHooks =
      [ className =? "Alacritty" --> queryMerge (className =? "Alacritty"), -- TODO: sometimes not working properly
        className =? "XTerm" --> queryMerge (className =? "XTerm") -- TODO: sometimes not working properly
      ]

myDynamicManageHook :: ManageHook
myDynamicManageHook =
  composeAll
    [ className =? "rambox" <&&> title =? "Rambox" --> defineBorderWidth 0,
      className =? "rambox" <&&> title =? "Rambox" --> doShift (show Sigma),
      className =? "Steam" <&&> title =? "Settings" --> doCustomCenterFloat -- TODO:  not detected!
      -- className =? "mage-client-MageFrame" <&&> title =? "Download symbols" --> doCenterFloat
    ]

myStartupHook :: X ()
myStartupHook = mapM_ spawnOnce spawnOnceCommands >> (modify $ \xstate -> xstate {windowset = onlyOnScreen 0 (show Sigma) (windowset xstate)}) >> (modify $ \xstate -> xstate {windowset = onlyOnScreen 1 (show Pi) (windowset xstate)}) >> setWMName "LG3D"
  where
    spawnOnceCommands =
      [ "lxqt-session",
        "udiskie -t",
        "nm-applet",
        "blueman-applet",
        -- "volumeicon",
        "mictray",
        "transmission-gtk -m",
        -- "emacs --daemon",
        -- "birdtray",
        -- "xscreensaver --no-splash",
        -- "transmission-daemon",
        -- "run-picom.sh",
        "mpd && sleep 10 && mpc shuffle",
        "dunst",
        "clipcatd",
        -- "xbanish",
        "change-random-wallpaper.sh",
        "run-alttab.sh",
        -- "run-trayer.sh",
        -- "run-gpsd.sh",
        -- "run-urserver.sh",
        "get-network-traffic.sh",
        "get-mpd-music.sh",
        "notify-clipcatd.sh",
        "notify-dgt-board.sh",
        "toggle-trackpad.sh disable",
        "mount-external-disk.sh",
        -- "thunderbird",
        -- "caprine",
        -- "slack",
        -- "rambox",
        "light-locker",
        "xset -dpms && xset s 0 0 && xset s off"
      ]

myTerminal :: String
myTerminal = "alacritty"

myWorkspaces :: [String]
myWorkspaces = show <$> [Alpha .. Omega]

myNamedScratchpads :: [NamedScratchpad]
myNamedScratchpads =
  [ NS termName spawnTerm findTerm manageTerm,
    NS fileManName spawnFileMan findFileMan manageFileMan,
    NS hTopName spawnHTop findHTop manageHTop,
    NS trashName spawnTrash findTrash manageTrash,
    NS plattenalbumName spawnPlattenalbum findPlattenalbum managePlattenalbum
  ]
  where
    termName = "scratchpad-terminal"
    termTitle = " -t " <> termName <> " "
    termClass = " --class " <> termName <> "," <> termName <> " "
    spawnTerm = myTerminal <> termTitle <> termClass
    findTerm = title =? termName
    manageTerm = doCustomCenterFloat
    --
    fileManName = "scratchpad-file-manager"
    fileManTitle = " -t " <> fileManName <> " "
    fileManClass = " --class " <> fileManName <> "," <> fileManName <> " "
    spawnFileMan = myTerminal <> fileManTitle <> fileManClass <> " -e " <> "xplr ~"
    findFileMan = title =? fileManName
    manageFileMan = doCustomCenterFloat
    --
    hTopName = "scratchpad-htop"
    hTopTitle = " -t " <> hTopName <> " "
    hTopClass = " --class " <> hTopName <> "," <> hTopName <> " "
    spawnHTop = myTerminal <> hTopTitle <> hTopClass <> " -e " <> "zenith"
    findHTop = title =? hTopName
    manageHTop = doCustomCenterFloat
    --
    trashName = "Trash"
    spawnTrash = "pcmanfm-qt trash://"
    findTrash = title =? trashName <&&> className =? "pcmanfm-qt"
    manageTrash = doCustomCenterFloat
    --
    plattenalbumName = "plattenalbum"
    spawnPlattenalbum = "plattenalbum"
    findPlattenalbum = className =? plattenalbumName
    managePlattenalbum = doCustomCenterFloat

--

data GreekWorkspace
  = Alpha
  | Beta
  | Delta
  | Lambda
  | Pi
  | Sigma
  | Omega
  deriving (Eq, Ord, Enum, Bounded)

instance Show GreekWorkspace where
  show ws = case ws of
    Alpha -> "\983083"
    Beta -> "\983201"
    Delta -> "\983490"
    Lambda -> "\984615"
    Pi -> "\984063"
    Sigma -> "\984224"
    Omega -> "\984009"

type HexColor = String

data DesktopColorTheme = ArcDark
  { base :: HexColor,
    text :: HexColor,
    background :: HexColor,
    foreground :: HexColor,
    tooltipBackground :: HexColor,
    tooltipForeground :: HexColor,
    selectedBackground :: HexColor,
    selectedForeground :: HexColor,
    insensitiveBackground :: HexColor,
    insensitiveForeground :: HexColor,
    notebookBackground :: HexColor,
    darkSidebarBackground :: HexColor,
    link :: HexColor,
    menuBackground :: HexColor
  }

-- doCustomCenterFloat :: ManageHook
-- doCustomCenterFloat = customFloating centerFloatPosition

doAbsoluteCenterFloat :: ManageHook
doAbsoluteCenterFloat = do
  -- Retrieve window properties
  windowId <- ask
  -- Set window to float mode (important for movable windows)
  liftX $ windows $ W.float windowId (W.RationalRect 0 0 1 1)
  -- Get screen dimensions
  screenWidth <- liftX $ gets (rect_width . screenRect . W.screenDetail . W.current . windowset)
  screenHeight <- liftX $ gets (rect_height . screenRect . W.screenDetail . W.current . windowset)
  liftX . io . logToFile $ "sw:" <> show screenWidth <> " sh:" <> show screenHeight
  -- Get window dimensions
  winAttr <- liftX $ withDisplay $ \dpy -> io $ getWindowAttributes dpy windowId
  let winWidth = fromIntegral $ wa_width winAttr
      winHeight = fromIntegral $ wa_height winAttr
  liftX . io . logToFile $ "ww:" <> show winWidth <> " wh:" <> show winHeight
  -- Calculate the center position
  let centerX = fromIntegral $ ((screenWidth - winWidth) `div` 2)
      centerY = fromIntegral $ ((screenHeight - winHeight) `div` 2)
  liftX . io . logToFile $ "cx:" <> show centerX <> " cy:" <> show centerY
  -- Move the window to the calculated center position
  liftX $ keysMoveWindowTo (centerX, centerY) (1 % 2, 1 % 2) windowId
  -- Allow the window to be handled by the next manage hook
  doF id

type LogMessage = String

logToFile :: LogMessage -> IO ()
logToFile s = withFile "/home/macalimlim/.xmonad/xmonad.log" AppendMode $ \h -> hPutStrLn h s

getRootWindowDimensions :: X ()
getRootWindowDimensions = do
  dpy <- asks display
  root <- asks theRoot
  (_, _, _, width, height, _, _) <- liftIO $ getGeometry dpy root
  io . logToFile $ "Root window dimensions: " <> show width <> "x" <> show height

{-
getCurrentScreenDimensions :: X ()
getCurrentScreenDimensions = withWindowSet $ \ws -> do
  let screen = W.screen (W.current ws) -- Get the currently selected screen
      screenRect = screenRect (W.screenDetail (W.current ws)) -- Get screen dimensions
  let (Rectangle x y width height) = screenRect
  -- Output or use the dimensions (you can log them, or use them in some way)
  io . logToFile $ "Screen dimensions: " ++ show (x, y, width, height)
-}

doCustomCenterFloat :: ManageHook
doCustomCenterFloat = doFloatDep move
  where
    move _ = centerFloatPosition

arcDark :: DesktopColorTheme
arcDark =
  ArcDark
    { base = "#404552",
      text = "#d3dae3",
      background = "#383c4a",
      foreground = "#d3dae3",
      tooltipBackground = "#4B5162",
      tooltipForeground = "#ffffff",
      selectedBackground = "#5294e2",
      selectedForeground = "#ffffff",
      insensitiveBackground = "#3e4350",
      insensitiveForeground = "#7c818c",
      notebookBackground = "#404552",
      darkSidebarBackground = "#353945",
      link = "#5294e2",
      menuBackground = "#383c4a"
    }

colorTheme :: DesktopColorTheme
colorTheme = arcDark

myFont :: String
myFont = "xft:SauceCodePro Nerd Font:regular:size=10:antialias=true:hinting=true"

tabTheme :: Theme
tabTheme =
  def
    { activeBorderColor = selectedBackground colorTheme,
      activeColor = selectedBackground colorTheme,
      activeTextColor = selectedForeground colorTheme,
      decoHeight = 20,
      fontName = myFont,
      inactiveBorderColor = insensitiveBackground colorTheme,
      inactiveColor = background colorTheme,
      inactiveTextColor = insensitiveForeground colorTheme
    }

swnConfig :: SWNConfig
swnConfig =
  def
    { swn_font = "xft:SauceCodePro Nerd Font Mono:bold:size=100",
      swn_fade = 1.0,
      swn_bgcolor = selectedBackground colorTheme,
      swn_color = selectedForeground colorTheme
    }

titleBarTheme :: Theme
titleBarTheme =
  def
    { activeBorderColor = selectedBackground colorTheme,
      activeColor = selectedBackground colorTheme,
      activeTextColor = selectedForeground colorTheme,
      decoHeight = 20,
      fontName = myFont,
      inactiveBorderColor = insensitiveBackground colorTheme,
      inactiveColor = background colorTheme,
      inactiveTextColor = insensitiveForeground colorTheme,
      windowTitleAddons =
        [ (" \61611", AlignLeft),
          ("\62161", AlignRightOffset minimizeButtonOffset),
          ("\62160", AlignRightOffset maximizeButtonOffset),
          ("\62163", AlignRightOffset closeButtonOffset)
        ]
    }
  where
    minimizeButtonOffset = 50
    maximizeButtonOffset = 30
    closeButtonOffset = 12

gsConfig :: GSConfig Window
gsConfig =
  defaultGsConfig
    { gs_cellheight = 30,
      gs_cellwidth = 100,
      gs_font = myFont
    }
  where
    colorizer =
      colorRangeFromClassName
        (0x38, 0x3c, 0x4a) -- lowest inactive bg
        (0x38, 0x3c, 0x4a) -- highest inactive bg
        (0x52, 0x94, 0xe2) -- active bg
        (0x7c, 0x81, 0x8c) -- inactive fg
        (0xff, 0xff, 0xff) -- active fg
    defaultGsConfig = buildDefaultGSConfig colorizer

emConfig :: HexColor -> EasyMotionConfig
emConfig c =
  def
    { bgCol = c,
      borderCol = c,
      cancelKey = xK_Escape,
      emFont = "xft: SauceCodePro Nerd Font-40",
      overlayF = textSize,
      sKeys = AnyKeys [xK_1, xK_2, xK_3, xK_4, xK_5, xK_6, xK_7, xK_8, xK_9, xK_0]
    }

redEmConfig = emConfig "#F92672"

blueEmConfig = emConfig $ selectedBackground colorTheme

showKeybinds :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybinds kb = noName $ io $ do
  h <- spawnPipe "rofi-keybinds-prompt.sh"
  hPutStr h (unlines $ showKmSimple kb)
  hClose h
  return ()

centerFloatPosition :: W.RationalRect
centerFloatPosition = W.RationalRect (1 / 6) (1 / 6) (2 / 3) (2 / 3)

toggleFloat :: Window -> X ()
toggleFloat w =
  windows
    ( \s ->
        if M.member w (W.floating s)
          then W.sink w s
          else W.float w centerFloatPosition s
    )

refreshXMonad :: X ()
refreshXMonad = refresh >> rescreen

reloadXMonad :: X ()
reloadXMonad = spawn "xmonad --recompile && xmonad --restart"

rsConfig :: RescreenConfig
rsConfig =
  RescreenConfig
    { afterRescreenHook = spawn "run-picom.sh" >> spawn "xrandr --output eDP1 --primary" >> spawn "change-random-wallpaper.sh",
      -- randrChangeHook = spawn "xrandr --setprovideroutputsource 1 0" >> spawn "autorandr --change",
      randrChangeHook = spawn "autorandr --change"
    }

queryMerge :: (Monoid b) => Query Bool -> Query b
queryMerge pGrp = do
  w <- ask
  aws <-
    liftX $
      filterM (runQuery pGrp)
        =<< gets
          (W.integrate' . W.stack . W.workspace . W.current . windowset)
  liftX $ windows (W.insertUp w)
  mapM_ (liftX . sendMessage . Merge w) aws
  idHook
